package kvstore

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	r := m.Run()
	os.RemoveAll("./filesystem_test")
	os.RemoveAll("./default.etcd")
	os.Exit(r)
}

func testKVS(kvs KVStore, t *testing.T) {
	// SAVE
	err := kvs.Save("test_delete_me", "test_delete_me_value")
	if err != nil {
		t.Fatal(err)
	}
	err = kvs.Save("test_should_exist", "bad_value")
	if err != nil {
		t.Fatal(err)
	}
	err = kvs.Save("test_should_exist", "test_should_exist_value")
	if err != nil {
		t.Fatal(err)
	}
	// LOAD
	val, _ := kvs.Load("test_should_exist", "default_value")
	if val != "test_should_exist_value" {
		t.Fatal("Value is not what it should be: '", val, "'")
	}
	val, _ = kvs.Load("test_should_not_exist", "default_value")
	if val != "default_value" {
		t.Fatal("Value is not what it should be: '", val, "'")
	}
	// DELETE
	err = kvs.Delete("test_delete_me")
	if err != nil {
		t.Fatal(err)
	}
	val, _ = kvs.Load("test_delete_me", "default_value")
	if val != "default_value" {
		t.Fatal("Value is not what it should be: '", val, "'")
	}
	kvs.Close()
}

func TestFilesystem(t *testing.T) {
	kvs, err := InitFilesystem("./filesystem_test/withsubfolder", true, 0700, 0700)
	if err != nil {
		t.Fatal(err)
	}
	testKVS(kvs, t)
}

//func TestPostgres(t *testing.T) {
//	kvs, err := InitPostgres("postgres://kvstore_test@localhost:5432/kvstore_test", "kvstore_test", true)
//	if err != nil {
//		t.Fatal(err)
//	}
//	testKVS(kvs, t)
//}

//func TestEtcd(t *testing.T) {
//	kvs, err := InitEtcd([]string{"127.0.0.1:2379"}, "/kvstoretest/")
//	if err != nil {
//		t.Fatal(err)
//	}
//	testKVS(kvs, t)
//}

// func TestS3(t *testing.T) {
//	kvs, err := InitS3("renshep-kvstore-test", "testing/")
//	if err != nil {
//		t.Fatal(err)
//	}
//	testKVS(kvs, t)
//}

//func TestDynamoDB(t *testing.T) {
//	kvs, err := InitDynamoDB("kvstore-test")
//	if err != nil {
//		t.Fatal(err)
//	}
//	testKVS(kvs, t)
//}
