package kvstore

import (
	"crypto/sha256"
	"encoding/hex"
	"io/ioutil"
	"os"
)

type kvstore_filesystem struct {
	StoragePath string
	FileMode    os.FileMode
}

func InitFilesystem(path string, createPath bool, pathMode os.FileMode, fileMode os.FileMode) (pkvs *kvstore_filesystem, err interface{}) {
	err = nil
	pkvs = nil
	var kvs kvstore_filesystem
	kvs.StoragePath = path
	kvs.FileMode = fileMode
	if createPath {
		err = os.MkdirAll(path, pathMode)
		if err != nil {
			return
		}
	}
	pkvs = &kvs
	return
}

func getKeyHash(key string) string {
	h := sha256.New()
	h.Write([]byte(key))
	return hex.EncodeToString(h.Sum(nil))
}

func (kvs *kvstore_filesystem) Save(key, value string) (err interface{}) {
	path := kvs.StoragePath + "/" + getKeyHash(key)
	return ioutil.WriteFile(path, []byte(value), kvs.FileMode)
}

func (kvs *kvstore_filesystem) Load(key, default_value string) (value string, err interface{}) {
	path := kvs.StoragePath + "/" + getKeyHash(key)
	data, rferr := ioutil.ReadFile(path)
	if rferr != nil {
		return default_value, rferr
	}
	return string(data), nil
}

func (kvs *kvstore_filesystem) Delete(key string) (err interface{}) {
	path := kvs.StoragePath + "/" + getKeyHash(key)
	return os.Remove(path)
}

func (kvs *kvstore_filesystem) Close() (err interface{}) {
	return nil
}
