#!/bin/bash
cd "${BASH_SOURCE%/*}"
echo "update $PWD"
echo 'enter to continue'
read
cp -f go.mod.latest go.mod &&
rm -rf vendor &&
go mod tidy &&
go mod vendor &&
echo done
