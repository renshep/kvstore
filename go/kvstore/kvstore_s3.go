package kvstore

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io"
	"strings"
)

type kvstore_s3 struct {
	svc        *s3.S3
	bucketName string
	keyPrefix  string
}

// uses default aws credential loading i.e. AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN AWS_REGION
// the table must already exist with primary key "key"
func InitS3(bucketName, keyPrefix string) (pkvs *kvstore_s3, err interface{}) {
	err = nil
	pkvs = nil
	var kvs kvstore_s3

	kvs.svc = s3.New(session.New())
	kvs.bucketName = bucketName
	kvs.keyPrefix = keyPrefix

	pkvs = &kvs
	return
}

func (kvs *kvstore_s3) Save(key, value string) (err interface{}) {
	err = nil
	input := &s3.PutObjectInput{
		Body:                 aws.ReadSeekCloser(strings.NewReader(value)),
		Bucket:               aws.String(kvs.bucketName),
		Key:                  aws.String(kvs.keyPrefix + key),
		ServerSideEncryption: aws.String("AES256"),
	}
	_, err = kvs.svc.PutObject(input)
	return
}

func (kvs *kvstore_s3) Load(key, default_value string) (value string, err interface{}) {
	value = default_value
	err = nil
	input := &s3.GetObjectInput{
		Bucket: aws.String(kvs.bucketName),
		Key:    aws.String(kvs.keyPrefix + key),
	}
	res, reserr := kvs.svc.GetObject(input)
	if reserr != nil {
		return default_value, reserr
	}
	defer res.Body.Close()
	data, readerr := io.ReadAll(res.Body)
	if readerr != nil {
		return default_value, readerr
	}
	return string(data), nil
}

func (kvs *kvstore_s3) Delete(key string) (err interface{}) {
	err = nil
	input := &s3.DeleteObjectInput{
		Bucket: aws.String(kvs.bucketName),
		Key:    aws.String(kvs.keyPrefix + key),
	}
	_, err = kvs.svc.DeleteObject(input)
	return
}

func (kvs *kvstore_s3) Close() (err interface{}) {
	return nil
}
